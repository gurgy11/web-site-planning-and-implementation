function rosterSelect() {
    var sel = document.getElementById("rosterSelect");
    var selRoster = sel.options[sel.selectedIndex].value;
    var containers = document.getElementsByClassName("roster-container");

    for (var i = 0; i < containers.length; i++) {
        if (containers[i].id == selRoster) {
            containers[i].removeAttribute("hidden", "");
        } else {
            containers[i].setAttribute("hidden", "");
        }
    }
}